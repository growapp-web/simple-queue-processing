# Generic wrappers for processing items in a background
A simple queue is a central entity in this concept. Someone puts items in a queue, and workers in the background process these items later. Workers get items from the queue in parallel.
The broker must orchestrate workers and maintain the correct operation of the system. 
Please consider these principles if you decide to write your own implementation.

## InMemory implementation
To use this implementation you need to implement `ISimpleQueueWorker<TItem>` and specify it in the config.
In the almost all cases will be enough to derive your worker from `SimpleQueueWorker`. 

Example usage:
```csharp
    public class SimpleIntWorker : SimpleQueueWorker<int>
    {
        public SimpleIntWorker(IServiceProvider serviceProvider) : base(serviceProvider) { }

        public override Task ProcessItems(int[] items)
        {
            // Do something
            return Task.CompletedTask;
        }
    }

    public class Program
    {
        public void Main()
        {
            var provider = new ServiceCollection()
                .AddTransient<SimpleIntWorker>()
                .AddInMemorySimpleQueue<int>()
                    .WithDefaultBroker("YourBrokerName")
                    .WithWorker(u => u.GetService<SimpleIntWorker>())
                .BuildServiceProvider();

            using var scope = provider.CreateScope();
            var broker = scope.ServiceProvider.GetService<ISimpleQueueBroker<int>>();

            for (int i = 0; i < 10_000_000; i++)
                broker.QueueItem(i);
        }
    }
```

### Logging
To receive logs just register `IProcessLogger<TItem>` implementation in the DI.
