﻿using Ga.SimpleQueueProcessing.Abstractions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Ga.SimpleQueueProcessing.InMemory.Tests.Mock
{
    public class ContainerLoggerMock<T> : IProcessLogger<T>
    {
        private readonly List<string> _infoMessagesContainer = new List<string>();
        private readonly List<string> _errorsMessagesContainer = new List<string>();
        private readonly List<string> _criticalMessagesContainer = new List<string>();
        private readonly List<string> _warningMessagesContainer = new List<string>();

        public ContainerLoggerMock()
        {
        }

        public void LogCritical(string message, Exception exception = null, [CallerMemberName] string callingMethod = "", [CallerLineNumber] int callingLineNumber = 0)
        {
            _criticalMessagesContainer.Add(message);
        }

        public void LogError(string message, Exception exception = null, [CallerMemberName] string callingMethod = "", [CallerLineNumber] int callingLineNumber = 0)
        {
            _errorsMessagesContainer.Add(message);
        }

        public void LogInfo(string message, [CallerMemberName] string callingMethod = "", [CallerLineNumber] int callingLineNumber = 0)
        {
            _infoMessagesContainer.Add(message);
        }

        public void LogWarning(string message, [CallerMemberName] string callingMethod = "", [CallerLineNumber] int callingLineNumber = 0)
        {
            _warningMessagesContainer.Add(message);
        }

        public void AssertHasErrors()
        {
            Assert.IsNotEmpty(_errorsMessagesContainer);
        }

        public void AssertHasCriticals()
        {
            Assert.IsNotEmpty(_errorsMessagesContainer);
        }
    }
}
