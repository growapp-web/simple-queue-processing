﻿using Ga.SimpleQueueProcessing.Abstractions;
using System;
using System.Runtime.CompilerServices;

namespace Ga.SimpleQueueProcessing.InMemory
{
    internal class BlackholeLogger<T> : IProcessLogger<T>
    {
        public void LogCritical(string message, Exception exception = null, [CallerMemberName] string callingMethod = "", [CallerLineNumber] int callingLineNumber = 0)
        {
        }

        public void LogError(string message, Exception exception = null, [CallerMemberName] string callingMethod = "", [CallerLineNumber] int callingLineNumber = 0)
        {
        }

        public void LogInfo(string message, [CallerMemberName] string callingMethod = "", [CallerLineNumber] int callingLineNumber = 0)
        {
        }

        public void LogWarning(string message, [CallerMemberName] string callingMethod = "", [CallerLineNumber] int callingLineNumber = 0)
        {
        }
    }
}
