using Ga.SimpleQueueProcessing.Abstractions;
using Ga.SimpleQueueProcessing.InMemory.Configuration;
using Ga.SimpleQueueProcessing.InMemory.Tests.Mock;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ga.SimpleQueueProcessing.InMemory.Tests
{
    public class ProcessingTests
    {
        [Test, Parallelizable]
        public async Task PullItemsInQueue_DefaultDIConfiguration_EachItemProcessedOnlyOnce()
        {
            var processedItems = new List<int>();

            var provider = new ServiceCollection()
                .AddTransient(u => new SimpleWorkerMock(u, processedItems))
                .AddInMemorySimpleQueue<int>()
                .WithDefaultBroker("UnitTestBroker")
                .WithWorker(u => u.GetService<SimpleWorkerMock>())
                .BuildServiceProvider();

            using var scope = provider.CreateScope();
            var broker = scope.ServiceProvider.GetService<ISimpleQueueBroker<int>>();

            for (int i = 0; i < 10_000_000; i++)
                broker.QueueItem(i);

            while (true)
            {
                await Task.Delay(TimeSpan.FromSeconds(1));

                var state = broker.GetBrokerState();
                if (state.QueueLength == 0)
                    break;
            }

            var duplicates = processedItems.GroupBy(u => u).Where(u => u.Count() > 1).Select(u => u.Key).ToArray();
            Assert.IsEmpty(duplicates, $"There are items processed more than once. Items: {string.Join('|', duplicates)}");
        }

        [Test, Parallelizable]
        public async Task CrushWorker_DefaultDIConfiguration_BrokenStillWorksWithAnotherWorkers()
        {
            var workersPool = new List<CrushableWorkerMock<int>>();

            var provider = new ServiceCollection()
                .AddTransient(u =>
                {
                    var newWorker = new CrushableWorkerMock<int>();
                    workersPool.Add(newWorker);
                    return newWorker;
                })
                .AddInMemorySimpleQueue<int>()
                .WithDefaultBroker("UnitTestBroker")
                .WithWorker(u => u.GetService<CrushableWorkerMock<int>>())
                .BuildServiceProvider();

            using var scope = provider.CreateScope();
            var broker = scope.ServiceProvider.GetService<ISimpleQueueBroker<int>>();

            var workerForCrush = workersPool.First();

            for (int i = 0; i < 10_000_000; i++)
                broker.QueueItem(i);

            workerForCrush.TurnOnCrushMode();

            await Task.Delay(TimeSpan.FromSeconds(1));

            var state = broker.GetBrokerState();
            Assert.AreEqual(true, state.BrokerIsRunning, "Broker stopped after worker crush");
        }

        [Test, Parallelizable]
        public void PullItemsWithError_DefaultDIConfiguration_LoggerWriteError()
        {
            var logger = new ContainerLoggerMock<int>();
            var provider = new ServiceCollection()
                .AddTransient(u => new ExceptionWorkerMock(u))
                .AddSingleton<IProcessLogger<int>>(logger)
                .AddInMemorySimpleQueue<int>()
                .WithDefaultBroker("UnitTestBroker")
                .WithWorker(u => u.GetService<ExceptionWorkerMock>())
                .BuildServiceProvider();

            using var scope = provider.CreateScope();
            var broker = scope.ServiceProvider.GetService<ISimpleQueueBroker<int>>();

            for (int i = 0; i < 10_000_000; i++)
                broker.QueueItem(i);

            logger.AssertHasErrors();
            logger.AssertHasCriticals();
        }
    }

}