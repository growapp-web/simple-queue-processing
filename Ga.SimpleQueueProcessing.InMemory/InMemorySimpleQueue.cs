﻿using Ga.SimpleQueueProcessing.Abstractions;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Ga.SimpleQueueProcessing.InMemory
{
    internal class InMemorySimpleQueue<T> : ISimpleQueue<T>
    {
        private readonly ConcurrentQueue<T> _items = new ConcurrentQueue<T>();
        private readonly int _batchSize;

        public int Length => _items.Count;

        public InMemorySimpleQueue(int batchSize = 150)
        {
            _batchSize = batchSize;
        }

        public void Enqueue(T item)
        {
            _items.Enqueue(item);
        }

        public IEnumerable<T> DequeueItems()
        {
            for (int i = 0; i < _batchSize; i++)
            {
                if (_items.TryDequeue(out var message))
                    yield return message;
            }
        }
    }
}
