﻿namespace Ga.SimpleQueueProcessing.Abstractions.Common
{
    public enum WorkerStates
    {
        None,
        IsRunning,
        Stopped,
        Fallen
    }
}
