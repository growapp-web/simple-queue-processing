﻿using Ga.SimpleQueueProcessing.Abstractions.Common;

namespace Ga.SimpleQueueProcessing.Abstractions
{
    public interface ISimpleQueueBroker<TItem>
    {
        HealthcheckState GetBrokerState();

        void QueueItem(TItem item);
    }
}
