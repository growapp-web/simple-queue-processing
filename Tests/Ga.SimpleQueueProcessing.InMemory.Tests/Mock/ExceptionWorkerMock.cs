﻿using System;
using System.Threading.Tasks;

namespace Ga.SimpleQueueProcessing.InMemory.Tests.Mock
{
    public class ExceptionWorkerMock : SimpleQueueWorker<int>
    {
        public ExceptionWorkerMock(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public override Task ProcessItems(int[] items)
        {
            throw new Exception("Test exception");
        }
    }
}
