﻿using Ga.SimpleQueueProcessing.Abstractions;
using Ga.SimpleQueueProcessing.Abstractions.Common;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Ga.SimpleQueueProcessing.InMemory.Tests.Mock
{
    public class CrushableWorkerMock<TItem> : ISimpleQueueWorker<TItem>
    {
        public WorkerStates State { get; private set; }
        public Guid WorkerId { get; private set; } = Guid.NewGuid();

        private Task _workerTask;
        private CancellationToken _cancellationToken;
        private ISimpleQueue<TItem> _itemsQueue;

        private bool _crushMode = false;

        public event Action<Guid> WorkerHasFallen;

        public Task ProcessItems(TItem[] items)
        {
            if (_crushMode)
                throw new Exception("Crush mode");

            return Task.CompletedTask;
        }

        public void TurnOnCrushMode()
        {
            State = WorkerStates.Fallen;
            _crushMode = true;
        }

        public void Start(ISimpleQueue<TItem> itemsQueue, CancellationToken? cancellationToken = null)
        {
            if (State == WorkerStates.IsRunning)
            {
                var exception = new InvalidOperationException("Worker already running");
                throw exception;
            }

            State = WorkerStates.IsRunning;

            _cancellationToken = cancellationToken ?? CancellationToken.None;
            _itemsQueue = itemsQueue;
            _workerTask = Task.Factory
                .StartNew(ListenQueue, _cancellationToken, TaskCreationOptions.LongRunning | TaskCreationOptions.DenyChildAttach, TaskScheduler.Default)
                .Unwrap();

            _workerTask.ContinueWith(u =>
            {
                State = WorkerStates.Fallen;

                WorkerHasFallen(WorkerId);
            }, TaskContinuationOptions.OnlyOnFaulted);

        }

        public void Stop()
        {
            State = WorkerStates.Stopped;
        }

        private async Task ListenQueue()
        {
            while (!_cancellationToken.IsCancellationRequested && State == WorkerStates.IsRunning)
            {
                if (_crushMode)
                    throw new Exception("Crush mode");

                try
                {
                    var items = _itemsQueue.DequeueItems().ToArray();

                    if (items.Length == 0)
                    {
                        await Task.Delay(500);
                        continue;
                    }

                    await ProcessItems(items);
                }
                catch (Exception ex)
                {
                }
            }
        }
    }
}
