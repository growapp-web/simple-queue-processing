﻿using Ga.SimpleQueueProcessing.Abstractions.Common;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Ga.SimpleQueueProcessing.Abstractions
{
    public interface ISimpleQueueWorker<TItem>
    {
        WorkerStates State { get; }
        Guid WorkerId { get; }

        event Action<Guid> WorkerHasFallen;

        Task ProcessItems(TItem[] items);
        void Start(ISimpleQueue<TItem> itemsQueue, CancellationToken? cancellationToken = null);
        void Stop();
    }
}
