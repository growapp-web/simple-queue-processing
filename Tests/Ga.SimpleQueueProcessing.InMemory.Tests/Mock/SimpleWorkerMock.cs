﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ga.SimpleQueueProcessing.InMemory.Tests.Mock
{
    public class SimpleWorkerMock : SimpleQueueWorker<int>
    {
        private readonly List<int> _processedItems;

        public SimpleWorkerMock(IServiceProvider serviceProvider, List<int> container) : base(serviceProvider)
        {
            _processedItems = container;
        }

        public override Task ProcessItems(int[] items)
        {
            _processedItems.AddRange(items);
            return Task.CompletedTask;
        }
    }
}
