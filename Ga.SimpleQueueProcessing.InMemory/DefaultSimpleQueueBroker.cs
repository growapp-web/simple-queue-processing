﻿using Ga.SimpleQueueProcessing.Abstractions;
using Ga.SimpleQueueProcessing.Abstractions.Common;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Ga.SimpleQueueProcessing.InMemory
{
    internal class DefaultSimpleQueueBroker<TItem> : ISimpleQueueBroker<TItem>
    {
        public int WorkersCount => _workers.Count;
        public bool IsRunning { get; private set; }
        public string BrokerName { get; private set; }
        public int ConcurrencyLevel { get; set; } = 10;

        public readonly Guid BrokerId = Guid.NewGuid();

        private readonly IProcessLogger<TItem> _logger;
        private readonly ISimpleQueue<TItem> _itemsQueue;
        private readonly IServiceProvider _serviceProvider;
        private readonly CancellationTokenSource _cancelTokenSource = new CancellationTokenSource();
        private readonly ConcurrentDictionary<Guid, ISimpleQueueWorker<TItem>> _workers = new ConcurrentDictionary<Guid, ISimpleQueueWorker<TItem>>();

        private Task _workersMonitorTask;

        public DefaultSimpleQueueBroker(ISimpleQueue<TItem> itemsQueue, IServiceProvider serviceProvider, string brokerName)
        {
            _logger = serviceProvider.GetService<IProcessLogger<TItem>>() ?? new BlackholeLogger<TItem>();
            _itemsQueue = itemsQueue;
            _serviceProvider = serviceProvider;
            BrokerName = brokerName;
        }

        public HealthcheckState GetBrokerState()
        {
            return new HealthcheckState
            {
                BrokerName = BrokerName,
                BrokerId = BrokerId.ToString(),
                BrokerIsRunning = IsRunning,
                QueueLength = _itemsQueue.Length,
                WorkersCount = WorkersCount,
                WorkersCountLimit = ConcurrencyLevel,
                FallenWorkersCount = _workers.Count(u => u.Value.State == WorkerStates.Fallen),
                RunningWorkersCount = _workers.Count(u => u.Value.State == WorkerStates.IsRunning),
                StoppedWorkersCount = _workers.Count(u => u.Value.State == WorkerStates.Stopped),
                CheckedAt = DateTimeOffset.UtcNow
            };
        }

        public void Start()
        {
            _logger.LogInfo($"Try start '{BrokerName}'");

            if (IsRunning)
                return;

            IsRunning = true;

            PushWorkerAndRun();

            _workersMonitorTask = Task.Factory
                .StartNew(MonitorWorkers, _cancelTokenSource.Token, TaskCreationOptions.LongRunning | TaskCreationOptions.DenyChildAttach, TaskScheduler.Default)
                .Unwrap();

            _workersMonitorTask.ContinueWith(u =>
            {
                _logger.LogCritical($"'{BrokerName}' has stopped with a critical error", u.Exception);
                Stop(force: true);
            }, TaskContinuationOptions.OnlyOnFaulted);
        }

        public void Stop(bool force = false)
        {
            _logger.LogInfo($"Try stop '{BrokerName}'; force: {force}");

            if (!IsRunning)
                return;

            IsRunning = false;

            if (force)
                _cancelTokenSource.Cancel();

            foreach (var worker in _workers)
                worker.Value.Stop();

            _workers.Clear();
        }

        public void QueueItem(TItem item)
        {
            _itemsQueue.Enqueue(item);
        }

        public void Dispose()
        {
            _logger.LogInfo($"'{BrokerName}' has disposed");
            _cancelTokenSource.Cancel();
        }

        private async Task MonitorWorkers()
        {
            while (IsRunning)
            {
                await Task.Delay(1000);

                var fallen = _workers.Values.Where(u => u.State == WorkerStates.Stopped || u.State == WorkerStates.Fallen);
                foreach (var item in fallen)
                {
                    _workers.TryRemove(item.WorkerId, out var _);
                    _logger.LogWarning($"Removed broken worker {item.WorkerId}; State: {item.State}; Broker: {BrokerName}");
                }

                if (!_workers.Values.Any(u => u.State == WorkerStates.IsRunning))
                {
                    _logger.LogCritical($"There is no active workers in broker {BrokerName}");
                    PushWorkerAndRun();
                    continue;
                }

                if (_itemsQueue.Length > 10000 && _workers.Count < ConcurrencyLevel)
                {
                    _logger.LogWarning($"Too large queue ({_workers.Count}). Try add workers. Current count {_workers.Count}");
                    PushWorkerAndRun();
                }
            }
        }

        private void PushWorkerAndRun()
        {
            _logger.LogInfo($"{BrokerName} try add worker");

            var worker = _serviceProvider.GetService<ISimpleQueueWorker<TItem>>();
            _workers.TryAdd(worker.WorkerId, worker);

            worker.WorkerHasFallen += WorkerHasFallen;
            worker.Start(_itemsQueue, _cancelTokenSource.Token);

            if (_workers.Count >= ConcurrencyLevel)
                _logger.LogWarning($"Max concurrency level ({ConcurrencyLevel}) has been reached for {BrokerName}");
        }

        private void WorkerHasFallen(Guid workerId)
        {
            _logger.LogError($"Handle worker fallen event. {workerId}");
            if (!_workers.TryRemove(workerId, out var worker))
                return;

            worker.WorkerHasFallen -= WorkerHasFallen;
            worker.Stop();
        }
    }
}
