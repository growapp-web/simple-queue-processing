﻿using System;
using System.Runtime.CompilerServices;

namespace Ga.SimpleQueueProcessing.Abstractions
{
    public interface IProcessLogger<TItem>
    {
        void LogInfo(string message, [CallerMemberName] string callingMethod = "", [CallerLineNumber] int callingLineNumber = 0);
        void LogWarning(string message, [CallerMemberName] string callingMethod = "", [CallerLineNumber] int callingLineNumber = 0);
        void LogError(string message, Exception exception = null, [CallerMemberName] string callingMethod = "", [CallerLineNumber] int callingLineNumber = 0);
        void LogCritical(string message, Exception exception = null, [CallerMemberName] string callingMethod = "", [CallerLineNumber] int callingLineNumber = 0);
    }
}
