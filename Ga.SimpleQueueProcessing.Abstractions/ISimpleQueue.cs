﻿using System.Collections.Generic;

namespace Ga.SimpleQueueProcessing.Abstractions
{
    public interface ISimpleQueue<T>
    {
        int Length { get; }

        void Enqueue(T item);
        IEnumerable<T> DequeueItems();
    }
}
