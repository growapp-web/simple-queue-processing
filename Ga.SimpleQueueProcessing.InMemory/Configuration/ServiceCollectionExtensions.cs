﻿using Ga.SimpleQueueProcessing.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Ga.SimpleQueueProcessing.InMemory.Configuration
{
    public static class ServiceCollectionExtensions
    {
        public static SimpleQueueStorageSettings<T> AddInMemorySimpleQueue<T>(this IServiceCollection services, int batchSize = 150)
        {
            services.AddSingleton(u => new InMemorySimpleQueue<T>(batchSize));
            services.AddSingleton<ISimpleQueue<T>>(u => u.GetService<InMemorySimpleQueue<T>>());

            return new SimpleQueueStorageSettings<T>(services);
        }

        public class SimpleQueueStorageSettings<T>
        {
            private readonly IServiceCollection _services;

            internal SimpleQueueStorageSettings(IServiceCollection services)
            {
                _services = services;
            }

            public SimpleQueueBrokerSettings<T> WithDefaultBroker(string brokerName, int levelOfParallelizm = 10)
            {
                _services.AddSingleton(u =>
                {
                    var instance = new DefaultSimpleQueueBroker<T>(u.GetService<ISimpleQueue<T>>(), u, brokerName)
                    {
                        ConcurrencyLevel = levelOfParallelizm
                    };
                    instance.Start();

                    return instance;
                });

                _services.AddSingleton<ISimpleQueueBroker<T>>(u => u.GetService<DefaultSimpleQueueBroker<T>>());

                return new SimpleQueueBrokerSettings<T>(_services);
            }
        }

        public class SimpleQueueBrokerSettings<T>
        {
            private readonly IServiceCollection _services;

            internal SimpleQueueBrokerSettings(IServiceCollection services)
            {
                _services = services;
            }

            public IServiceCollection WithWorker(Func<IServiceProvider, ISimpleQueueWorker<T>> implementationFactory)
            {
                _services.AddTransient(implementationFactory);

                return _services;
            }
        }
    }
}
