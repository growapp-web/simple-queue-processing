﻿using System;

namespace Ga.SimpleQueueProcessing.Abstractions.Common
{
    public class HealthcheckState
    {
        public string BrokerName { get; set; }

        public string BrokerId { get; set; }

        public bool BrokerIsRunning { get; set; }

        public int QueueLength { get; set; }

        public int WorkersCount { get; set; }

        public int WorkersCountLimit { get; set; }

        public int FallenWorkersCount { get; set; }

        public int RunningWorkersCount { get; set; }

        public int StoppedWorkersCount { get; set; }

        public DateTimeOffset CheckedAt { get; set; }

    }
}
