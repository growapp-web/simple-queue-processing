﻿using Ga.SimpleQueueProcessing.Abstractions;
using Ga.SimpleQueueProcessing.Abstractions.Common;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Ga.SimpleQueueProcessing.InMemory
{
    public abstract class SimpleQueueWorker<TItem> : ISimpleQueueWorker<TItem>
    {
        public WorkerStates State { get; private set; }
        public Guid WorkerId { get; private set; } = Guid.NewGuid();

        protected readonly IProcessLogger<TItem> _logger;

        private Task _workerTask;
        private CancellationToken _cancellationToken;
        private ISimpleQueue<TItem> _itemsQueue;

        public SimpleQueueWorker(IServiceProvider serviceProvider)
        {
            _logger = serviceProvider.GetService<IProcessLogger<TItem>>() ?? new BlackholeLogger<TItem>();
        }

        public event Action<Guid> WorkerHasFallen;

        public abstract Task ProcessItems(TItem[] items);

        public void Start(ISimpleQueue<TItem> itemsQueue, CancellationToken? cancellationToken = null)
        {
            if (State == WorkerStates.IsRunning)
            {
                var exception = new InvalidOperationException("Worker already running");
                _logger.LogError("Failed to start worker", exception);
                throw exception;
            }

            State = WorkerStates.IsRunning;

            _cancellationToken = cancellationToken ?? CancellationToken.None;
            _itemsQueue = itemsQueue;
            _workerTask = Task.Factory
                .StartNew(ListenQueue, _cancellationToken, TaskCreationOptions.LongRunning | TaskCreationOptions.DenyChildAttach, TaskScheduler.Default)
                .Unwrap();

            _workerTask.ContinueWith(u =>
            {
                _logger.LogCritical($"Worker {WorkerId} completed with failure");

                State = WorkerStates.Fallen;

                var ex = u.Exception;
                if (ex != null)
                    _logger.LogCritical("SimpleQueueWorker error", ex);

                WorkerHasFallen(WorkerId);
            }, TaskContinuationOptions.OnlyOnFaulted);

            _logger.LogInfo($"Worker {WorkerId} has started");
        }

        public void Stop()
        {
            State = WorkerStates.Stopped;
            _logger.LogInfo($"Worker {WorkerId} has stopped");
        }

        private async Task ListenQueue()
        {
            while (!_cancellationToken.IsCancellationRequested && State == WorkerStates.IsRunning)
            {
                try
                {
                    var items = _itemsQueue.DequeueItems().ToArray();

                    if (items.Length == 0)
                    {
                        await Task.Delay(500);
                        continue;
                    }

                    await ProcessItems(items);
                }
                catch (Exception ex)
                {
                    _logger.LogError("Exception while processing items", ex);
                }
            }

            if (_cancellationToken.IsCancellationRequested)
                _logger.LogWarning($"Cancellation was requested. Worker {WorkerId}");
        }
    }
}
